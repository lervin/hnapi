import supertest from "supertest";
import express from "express";
import { loginRouter } from '../Routes/Login.route';
import dotenv from 'dotenv';

dotenv.config();
const app = express();
app.use(express.json());

app.use("/", loginRouter);

describe("POST /login", () => {
    test("responds with json", done => {
        supertest(app)
        .post('/login')
        .send({username: "lervin"})
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, done)
    });
});