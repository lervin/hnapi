import { isMonth } from "../utils/utils";

describe('Test utility functions', () => {
    test("Expected to be true", () => {
        expect(isMonth("January")).toBe(true);
    })
})