import { connect } from 'mongoose';

export const connectDB = () => {
    connect('mongodb://mongodb:27017')
    .then(() => {
        console.log('Database Connected');
    })
    .catch(err => console.log(err));
}
