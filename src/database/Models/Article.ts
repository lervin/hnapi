import { Schema, model } from 'mongoose';

const ArticleSchema = new Schema({ 
    created_at: String,
    title: String,
    url: String,
    author: String,
    points: Number,
    story_text: String,
    comment_text: String,
    num_comments: Number,
    story_id: Number,
    story_title: String, 
    story_url: String,
    parent_id: Number,
    created_at_id: Number,
    _tags: [String],
    objectID: String,
    _highlightResult: {
        author: { value: String, matchLevel: String, matchedWords: [String] },
        comment_text: { value: String, matchLevel: String, fullyHighlighted: Boolean, matchedWords: [String] },
        story_title: { value: String, matchLevel: String, matchedWords: [String] },
        story_url: { value: String, matchLevel: String, matchedWords: [String] },
    },
});

export const Article = model('Article', ArticleSchema);
