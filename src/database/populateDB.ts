import { Article } from './Models/Article';
import axios from 'axios';

export const getArticles = async (): Promise<void> => {
    const url: string = "https://hn.algolia.com/api/v1/search_by_date?query=nodejs"
    try {
        const res = await axios.get(url);
        saveToDB(res.data);
    } 
    catch(error) {
        console.log(error)
    }
};

const saveToDB = async ({ hits }: any): Promise<void> => {
    hits.forEach(async (hit: any) => {
        const objID = await Article.find({ objectID: hit.objectID }).exec();
        if(objID.length === 0) {
            const article = new Article({ 
                created_at: hit.created_at,
                title: hit.title,
                url: hit.url,
                author: hit.author,
                points: hit.points,
                story_text: hit.story_text,
                comment_text: hit.comment_text,
                num_comments: hit.num_comments,
                story_id: hit.story_id,
                story_title: hit.story_title, 
                story_url: hit.story_url,
                parent_id: hit.parent_id,
                created_at_id: hit.created_at_id,
                _tags: hit._tags,
                objectID: hit.objectID,
                _highlightResult: hit._highlightResult

             });
            await article.save();
            console.log(`Article ${hit.objectID} saved at time: ${Date.now()}`);
        }
    })
}
