import express, {Application} from 'express';
import { connectDB } from './database/connection';
import { router } from './Routes/Article.route';
import { authenticateToken } from './Routes/middlewares/Auth';
import { loginRouter } from './Routes/Login.route';
import dotenv from 'dotenv';
import swaggerUi from 'swagger-ui-express';
import swaggerDocument from './swagger.json';
import { getArticles } from './database/populateDB';

dotenv.config();
const app: Application = express();

//Database Connection
connectDB();

//Populate database
getArticles();

//Add new article every hour
setInterval(function() {
    getArticles();
}, 3600000);


//Configuration
const port: number | string = process.env.PORT || 4000;

//Middleware
app.use(express.json());

//Routes
app.use('/', loginRouter);
app.use('/api/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use('/api', authenticateToken, router);

app.listen(port, () => {
    console.log(`Server Running on port ${port}`);
})