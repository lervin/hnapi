import express, { Router } from 'express';
import * as controller from './Controllers/Article.controller';

export const router: Router  = express.Router();


router.get('/articles', controller.getArticles);
router.delete('/articles/:id', controller.deleteArticle);

