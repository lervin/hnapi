import jwt from 'jsonwebtoken';
import {Request, Response, NextFunction} from 'express'

export const authenticateToken = (req: Request, res: Response, next: NextFunction) => {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
  
    if (token == null) return res.sendStatus(401)
  
    jwt.verify(token, process.env.SECRET_TOKEN as string, (err: any, user: any) => {
      if (err) return res.sendStatus(403);
      next();
    })
  }
