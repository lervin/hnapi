import { Request, Response, Router } from 'express';
import jwt from 'jsonwebtoken';

export const loginRouter: Router = Router();

loginRouter.post('/login', (req: Request, res: Response) => {
    const username: string = req.body.username;
    const user: object = { username };

    const accessToken = jwt.sign(user, process.env.SECRET_TOKEN as string);
    res.json({accessToken: accessToken});
});