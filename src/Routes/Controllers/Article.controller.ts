import { Article } from '../../database/Models/Article';
import { Request, Response } from 'express';
import { isMonth } from '../../utils/utils';
import moment from 'moment';

export const getArticles = async (req: Request, res: Response) => {
    const { page = 1, limit = 5 } = req.query;
    const filterQuery: FilterQuery = {};

    if(req.query.author) {
        filterQuery.author = req.query.author as string;
    }
    if(req.query.month && isMonth(req.query.month as string)) {
        filterQuery.$expr = { $eq: [{$month :{$dateFromString: {dateString: "$created_at"}}}, +moment().month(req.query.month as string).format('M')] };
    }
    if(req.query._tags) {
        filterQuery._tags = { $all: (req.query._tags as string).split(',') as string[] };
    } 

    try { 
        const articles = await Article.find(filterQuery)
        .limit(+limit * 1)
        .skip((+page - 1) * +limit)
        .exec();
    
        const count = await Article.countDocuments();
    
        res.json({
            articles,
            totalPages: Math.ceil(count / +limit),
            currentPage: page
        });
    } catch(err) {
        console.log(err);
    }
}

export const deleteArticle = async (req: Request, res: Response) => {
    try { 
        await Article.findByIdAndDelete(req.params.id);
        res.json({ status: `Article ${req.params.id} Deleted` });
    } catch(err) {
        console.log(err);
    }

};

interface FilterQuery {
    author? : string ,
    $expr? : {
            $eq: [{ $month: { $dateFromString: { dateString: string; }; }; }, number]
    },
    _tags?: {
        $all: string | string[]
    },
}
